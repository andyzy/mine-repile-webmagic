package com.shz.appletsapi.common;

import com.shz.appletsapi.prop.ShzWebConstants;

public class ControllerResult {
	
	public final static ControllerResult SUCCESS = new ControllerResult();

	private String status = ShzWebConstants.STATUS_SUCCESS;
	
	private String msg;
	
	public ControllerResult() {}
	
	public ControllerResult(String status,String msg) {
		this.status = status;
		this.msg = msg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
