package com.shz.appletsapi.common.support;

public class GameType {
	public static int getType(String type) {
		int i=1;
		switch (type) {
		case "休闲益智":
			i=1;
			break;
		case "动作冒险":
			i=2;
			break;
		case "飞行射击":
			i=3;
			break;
		case "角色扮演":
			i=4;
			break;
		case "体育竞技":
			i=5;
			break;
		case "经营策略":
			i=6;
			break;
		case "跑酷竞速":
			i=7;
			break;
		case "棋牌扑克":
			i=8;
			break;
		case "辅助工具":
			i=9;
			break;
		default:
			return 1;
		}
		return i;
	}
}
