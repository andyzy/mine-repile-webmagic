package com.shz.appletsapi.prop;

import java.util.ArrayList;

public interface ShzDefaults {
	

	interface Async {
		int corePoolSize = 2;
		int maxPoolSize = 50;
		int queueCapacity = 10000;
		int keepAliveSeconds = 300;
	}
	
	interface Rest {
		int connectTimeout = 5000;
		int readTimeout = 5000;
		int connectRequestTimeout = 1000;
	}
	
	interface Reptile {
		String path = "./static";
		ArrayList<String> coinNames = new ArrayList<>();
		
	}


}
