package com.shz.appletsapi.prop;

import java.util.ArrayList;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;


@Configuration
@ConfigurationProperties("shz")
public class ShzProperties {

	
	@Getter
	private final Async async = new Async();
	@Getter
	private final Rest rest =new Rest();
	@Getter
	private final Reptile reptile =new Reptile();
	
	
	/**
	 * 异步线程设置
	 */
	@Getter@Setter
	public static class Async {
		private int corePoolSize = ShzDefaults.Async.corePoolSize;
		private int maxPoolSize = ShzDefaults.Async.maxPoolSize;
		private int queueCapacity = ShzDefaults.Async.queueCapacity;
		private int keepAliveSeconds = ShzDefaults.Async.keepAliveSeconds;
	}
	
	@Getter@Setter
	public static class Rest {
		private int connectTimeout=ShzDefaults.Rest.connectTimeout;
		private int readTimeout=ShzDefaults.Rest.readTimeout;
		private int connectRequestTimeout=ShzDefaults.Rest.connectRequestTimeout;
	}
	
	@Getter@Setter
	public static class Reptile {
		private String path = ShzDefaults.Reptile.path;
		private ArrayList<String> coinNames = ShzDefaults.Reptile.coinNames;
	}
	
	

}
