package com.shz.appletsapi.prop;

public interface ShzWebMsg {
	
	String MSG_SUCCESS = "操作成功";
	String MSG_FAILURE = "操作失败";
	
	String MSG_IDNULL="id不能为空";
	String MSG_STATUSNULL="状态不能为空";
	String MSG_APPCODENULL="密匙不能为空";
	String MSG_HEADIMGURLNULL="头像地址不能为空";
	String MSG_USERNAMENULL="用户名不能为空";
	
	String MSG_TYPENULL="类型不能为空";
	String MSG_SEARCHNULL="关键字不能为空";
	String MSG_SORTNULL="排序规则不能为空";
	String MSG_GAMEIDNULL="游戏id不能为空";
	
	
	String MSG_USER_ERROR="用户不存在";
	String MSG_GAME_ERROR="游戏不存在";
	
	
	String MSG_USER_USERCODE_NULL="用户标识不能为空";
	String MSG_USER_USERIMAGE_NULL="用户头像不能为空";
	String MSG_USER_USERNAME_NULL="用户名称不能为空";
	
	String MSG_USER_CHANNEL_NULL="渠道不能为空";
	
	String MSG_SCORE_NULL="评分不能为空";
	
	
	String MSG_USER_TYPE_THREE_ERROR="用户并没有关注此游戏";
}
