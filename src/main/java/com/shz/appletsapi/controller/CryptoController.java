package com.shz.appletsapi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shz.appletsapi.controller.result.PageResult;
import com.shz.appletsapi.model.po.mine.CryptoCoinz;
import com.shz.appletsapi.model.vo.Message_CN;
import com.shz.appletsapi.model.vo.Message_EN;
import com.shz.appletsapi.repository.mine.CryptoCoinzRepository;

@RequestMapping("/crypto")
@Controller
public class CryptoController {

	private final CryptoCoinzRepository cryptoCoinzRepository;
	private final HttpServletRequest request;



	public CryptoController(CryptoCoinzRepository cryptoCoinzRepository, HttpServletRequest request) {
		this.cryptoCoinzRepository = cryptoCoinzRepository;
		this.request = request;
	}

	@RequestMapping
	public String init() {
		return "system/crypto/index";
	}

	@RequestMapping("/list")
	@ResponseBody
	public PageResult<CryptoCoinz> list(String search,String message) {
		List<CryptoCoinz> list = new ArrayList<CryptoCoinz>();
		if (search == null) {
			list = cryptoCoinzRepository.findAll();
		} else {
			list = cryptoCoinzRepository.findByNameLike("%" + search + "%");
		}	
			return new PageResult<CryptoCoinz>(list.size(), list);
		
	}

	@RequestMapping("/message")
	@ResponseBody
	public Object message(String message) {
		if(message.equals("EN")) {
			return new Message_EN();
		}else  {
			return new Message_CN();
		}
			
	}

}
