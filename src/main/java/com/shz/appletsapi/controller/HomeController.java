package com.shz.appletsapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class HomeController {


	@RequestMapping("/hello")
	public String index(@RequestParam("file") MultipartFile file) {
		System.out.println("-----------"+file);
		return "hello world";
	}
	
	@GetMapping("/list")
	public Object list(){
		Map map =new HashMap<>(); 
		
		List<String> list =new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		
		List<String> list2 =new ArrayList<String>();
		list2.add("2018-6-15");
		list2.add("2018-6-16");
		list2.add("2018-6-17");
		list2.add("2018-6-18");
		list2.add("2018-6-19");
		list2.add("2018-6-20");
		list2.add("2018-6-21");
		
		List<String> list3 =new ArrayList<String>();
		list3.add("320");
		list3.add("321");
		list3.add("322");
		list3.add("323");
		list3.add("324");
		list3.add("325");
		list3.add("326");
		
		List<Map> listmap =new ArrayList<>();
		
		Map map2 =new HashMap<>();
		map2.put("name", "a");
		map2.put("type", "bar");
		map2.put("data", list3);
		
		listmap.add(map2);
		listmap.add(map2);
		listmap.add(map2);
		listmap.add(map2);
		listmap.add(map2);
		listmap.add(map2);
		listmap.add(map2);
		
		map.put("legend", list);
		map.put("xAxis", list2);
		map.put("series", listmap);
		return map;
	}
}
