package com.shz.appletsapi.config;

import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.shz.appletsapi.config.support.WxMappingJackson2HttpMessageConverter;
import com.shz.appletsapi.prop.ShzProperties;

@Configuration
public class RestTemplateConfiguration {
	
	@Autowired
	private ShzProperties shzProperties;
	
	
	@Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(HttpClients.createDefault());
        //这里是使用了自定义的一个HttpsClientPoolThread线程池单例 以后有机会会单独写文章展示其配置内容, 大家可以先使用默认的HttpClients.createDefault()进行配置,或自定义线程池;
        clientHttpRequestFactory.setConnectTimeout(shzProperties.getRest().getConnectTimeout());
        clientHttpRequestFactory.setReadTimeout(shzProperties.getRest().getReadTimeout());
        clientHttpRequestFactory.setConnectionRequestTimeout(shzProperties.getRest().getConnectRequestTimeout());
        return clientHttpRequestFactory;
    }
	
	@Bean
	public RestTemplate restTemplate(ClientHttpRequestFactory factory) {
		RestTemplate restTemplate =new RestTemplate();
		restTemplate.setRequestFactory(factory);
		//微信接口文档虽说返回的是 Json 数据，但是同时返回的 Header 里面的 Content-Type 值确是 text/plain 的！！
		restTemplate.getMessageConverters().add(new WxMappingJackson2HttpMessageConverter());
        return restTemplate;
    }

}
