package com.shz.appletsapi.config;

import java.util.concurrent.Executors;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;


/**
 * task并发
 * @author zy
 *
 */
@Configuration
public class ScheduleConfiguration implements SchedulingConfigurer {

	 @Override
	 public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
	        //设定一个长度10的定时任务线程池
	        taskRegistrar.setScheduler(Executors.newScheduledThreadPool(10));
	    }

	
}
