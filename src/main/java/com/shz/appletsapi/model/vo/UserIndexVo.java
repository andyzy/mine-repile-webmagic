package com.shz.appletsapi.model.vo;

import java.util.List;

import lombok.Data;

@Data
public class UserIndexVo {
	
	

	public UserIndexVo(List<UserGameVo> games) {
		this.games = games;
	}

	private List<UserGameVo> games;

}
