package com.shz.appletsapi.model.vo.search.feed;

import java.util.Date;

import lombok.Data;

@Data
public class FeedVo {
	
	private String theme;
	
	private String content;
	
	private Date time;

}
