package com.shz.appletsapi.model.vo;

import java.util.List;

import lombok.Data;

@Data
public class IndexVo {
	
	
	public IndexVo(List<GameVo> games) {
		this.games = games;
	}

	private List<GameVo> games;
	
	

}
