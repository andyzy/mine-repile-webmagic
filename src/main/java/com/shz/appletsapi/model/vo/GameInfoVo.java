package com.shz.appletsapi.model.vo;

import java.util.List;

import lombok.Data;

@Data
public class GameInfoVo {
	
	private String title;
	private String imageUrl;
	private String score;
	private String introduction;
	private String wxQrCode;
	private String screenshots;
	private String source;
	private String area;
	private String attentions;
	private Boolean isAttention;
    private List<EvaluationVo> evaluations; 
	
}
