package com.shz.appletsapi.model.vo;

import lombok.Data;

@Data
public class TypeVo {
	
	private String typeId;
	private String typeName;

}
