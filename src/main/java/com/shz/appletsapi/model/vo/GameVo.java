package com.shz.appletsapi.model.vo;

import lombok.Data;

@Data
public class GameVo {
	
	private String id;
	
	private String title;
	
	private String imageUrl;
	
	private String score;
	
	private String introduction;
	

}
