package com.shz.appletsapi.model.vo;

import java.util.Date;

import lombok.Data;

@Data
public class EvaluationVo {
	private String userName;
	private String userImageUrl;
	private Date time;
	private String score;
	private String content;
}
