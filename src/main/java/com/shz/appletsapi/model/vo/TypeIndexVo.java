package com.shz.appletsapi.model.vo;

import java.util.List;

import lombok.Data;

@Data
public class TypeIndexVo {
	private List<TypeVo> types;

}
