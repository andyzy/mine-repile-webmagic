package com.shz.appletsapi.model.vo.search;

import lombok.Data;

@Data
public class SearchVo {

	private String name;
	
}
