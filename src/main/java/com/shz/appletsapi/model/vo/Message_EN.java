package com.shz.appletsapi.model.vo;

import com.shz.appletsapi.model.vo.message.Message;

import lombok.Data;

@Data
public class Message_EN extends Message {
	
	private String name="Name/Algorithm";
	
	private String nodes="Nodes";
	
	private String price="Price $";
	
	private String priceBtc="Price BTC";
	
	private String roiDays="ROI Days";
	
	private String roiYearly="ROI Yearly";
	
	private String coinsForNode="Coins for Node";
	
	private String nodeWorth="Node Worth";
	
	private String coinsDaily="Coins Daily";
	
	private String daily="Daily/Weekly";
	
	private String monthly="Monthly/Yearly";
	

}
