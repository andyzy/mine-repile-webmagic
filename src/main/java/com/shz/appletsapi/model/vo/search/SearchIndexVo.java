package com.shz.appletsapi.model.vo.search;

import java.util.List;

import lombok.Data;

@Data
public class SearchIndexVo {
	
	
	
	public SearchIndexVo(List<SearchVo> searchs) {
		super();
		this.searchs = searchs;
	}

	private List<SearchVo> searchs;

}
