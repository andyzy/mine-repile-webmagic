package com.shz.appletsapi.model.vo;

import lombok.Data;

@Data
public class Message_CN  {
	
	private String name="名称/算法";
	
	private String nodes="节点数";
	
	private String price="价格 $";
	
	private String priceBtc="价格 BTC";
	
	private String roiDays="投资回报率日";
	
	private String roiYearly="投资回报率年";
	
	private String coinsForNode="节点的货币";
	
	private String nodeWorth="节点价值";
	
	private String coinsDaily="每日货币";
	
	private String daily="日/周";
	
	private String monthly="月/年";
	

}
