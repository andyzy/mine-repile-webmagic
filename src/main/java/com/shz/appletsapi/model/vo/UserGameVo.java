package com.shz.appletsapi.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserGameVo extends GameVo {
	
	private String content;
	private String createTime;

}
