package com.shz.appletsapi.model.vo.search.feed;

import java.util.List;

import lombok.Data;

@Data
public class FeedIndexVo {
	
	
	
	public FeedIndexVo(List<FeedVo> feeds) {
		this.feeds = feeds;
	}

	private List<FeedVo> feeds;
	
	

}
