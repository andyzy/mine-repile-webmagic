package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "coins")
public class Coin implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6990682035805321755L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "coin_code")
	private String coinCode; 
	
	@Column(name = "coin_logo")
	private String coinLogo; 
	
	@Column(name = "coin_en")
	private String coinEn; 
	
	@Column(name = "coin_cn")
	private String coinCn; 
	
	@Column(name = "price")
	private String price; 
	
	@Column(name = "price_day_max")
	private String priceDayMax; 
	
	@Column(name = "price_day_min")
	private String priceDayMin; 
	
	@Column(name = "gain")
	private String gain; 
	
	@Column(name = "des")
	private String des; 
	
	@Column(name = "coin_cmv")
	private String coinCmv; 
	
	@Column(name = "coin_cmv_rank")
	private String coinCmvRank; 
	
	@Column(name = "coin_total_c")
	private String coinTotalC; 
	
	@Column(name = "coin_total_c2")
	private String coinTotalC2; 
	
	@Column(name = "day_turnover")
	private String dayTurnover; 
	
	@Column(name = "day_turnover_rank")
	private String dayTurnoverRank; 
	
	@Column(name = "exchange")
	private String exchange; 
	
	@Column(name = "publish_time")
	private String publishTime; 
	
	@Column(name = "white_paper")
	private String whitePaper; 
	
	@Column(name = "create_time")
	private Date createTime; 
	
	@Column(name = "update_time")
	private Date updateTime; 
	
	

}
