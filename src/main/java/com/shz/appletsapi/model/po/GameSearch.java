package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "game_search")
public class GameSearch implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -5128787430005737659L;

/**
	 * 
	 */

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "search")
	private String search; 
	
	@Column(name = "count")
	private Integer count; 

	@Column(name = "create_time")
	private Date createTime;

}
