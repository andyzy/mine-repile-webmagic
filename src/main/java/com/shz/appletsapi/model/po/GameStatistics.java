package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "game_statistics")
public class GameStatistics implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -8943105172671682367L;
/**
	 * 
	 */

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "user_code")
	private String userCode; 
	@Column(name = "channel")
	private String channel; 
	@Column(name = "create_date")
	private Date createDate;
	@Column(name = "create_time")
	private Date createTime;

}
