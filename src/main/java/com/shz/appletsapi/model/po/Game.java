package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "game_info")
public class Game implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5721619020442643323L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "title")
	private String title; //标题
	@Column(name = "image_url")
	private String imageUrl; //简略图
	@Column(name = "tags")
	private String tags; //标签
	@Column(name = "type")
	private int type; //类型
	@Column(name = "wx_qr_code")
	private String wxQrCode; //小程序码
	@Column(name = "area")
	private String area; //地区
	@Column(name = "author")
	private String author; //作者
	@Column(name = "releaseTime")
	private String releaseTime; //发布时间
	@Column(name = "source")
	private String source; //来源
	@Column(name = "claim")
	private String claim; //要求
	@Column(name = "screenshots")
	private String screenshots;  //截图
	@Column(name = "introduction")
	private String introduction; //简介
	
	@Column(name = "ident")
	private Integer ident;
	
	@Column(name = "create_time")
	private Date createTime;
	@Column(name = "update_time")
	private Date updateTime;
	

}
