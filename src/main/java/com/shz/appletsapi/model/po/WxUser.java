package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "wx_user")
public class WxUser implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3560761052164223342L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	
	@Column(name = "user_code")
	private String userCode; 
	@Column(name = "user_image_url")
	private String userImageUrl; 
	@Column(name = "user_name")
	private String userName; 
	@Column(name = "create_time")
	private Date createTime; 
}
