package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "game_operating")
public class GameOperating implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 3164218718855124801L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "user_code")
	private String userCode; 
	
	@Column(name = "game_id")
	private Integer gameId;
	
	@Column(name = "type")
	private Integer type;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "score")
	private Integer score;
	
	@Column(name = "create_time")
	private Date createTime;

}
