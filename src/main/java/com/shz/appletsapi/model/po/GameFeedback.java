package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "game_feedback")
public class GameFeedback implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -8331582056085189043L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "user_code")
	private String userCode; 
	
	@Column(name = "contact")
	private String contact; 
	@Column(name = "theme")
	private String theme; 
	
	@Column(name = "content")
	private String content; 

	@Column(name = "create_time")
	private Date createTime;

}
