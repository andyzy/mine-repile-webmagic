package com.shz.appletsapi.model.po.mine;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.shz.appletsapi.model.vo.message.Message;

import lombok.Data;

@Data
@Entity
@Table(name = "crypto_coinz")
public class CryptoCoinz implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -680190147582700830L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "logo_src")
	private String logoSrc; 
	
	@Column(name = "name")
	private String name; 
	
	@Column(name = "diff")
	private String diff; 
	
	@Column(name = "nodes")
	private String nodes;
	
	@Column(name = "price")
	private String price;
	
	@Column(name = "price_btc")
	private String priceBtc;
	
	@Column(name = "roi_days")
	private String roiDays;
	
	@Column(name = "roi_yearly")
	private String roiYearly;
	
	@Column(name = "coins_for_node")
	private String coinsForNode;
	
	@Column(name = "node_worth")
	private String nodeWorth;
	
	@Column(name = "coins_daily")
	private String coinsDaily;
	
	@Column(name = "daily")
	private String daily;
	
	@Column(name = "weekly")
	private String weekly;
	
	@Column(name = "monthly")
	private String monthly;
	
	@Column(name = "yearly")
	private String yearly;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate;

}
