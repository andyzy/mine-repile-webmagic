package com.shz.appletsapi.model.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "news")
public class News implements Serializable {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = -6064372636071830363L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "title")
	private String title; 
	
	@Column(name = "author")
	private String author; 
	
	@Column(name = "logo")
	private String logo; 
	
	@Column(name = "original_link")
	private String originalLink; 
	
	@Column(name = "original_content")
	private String originalContent; 
	
	@Column(name = "publish_time")
	private String publishTime; 
	
	@Column(name = "create_time")
	private Date createTime; 
	
	@Column(name = "create_date")
	private Date createDate; 
	
	

}
