package com.shz.appletsapi.model.po.mine;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "coins")
public class Coins implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1819399009153946653L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "coin_name")
	private String coinName; 
	
	@Column(name = "price")
	private String price; 
	
	@Column(name = "create_time")
	private Date createTime; 
	
	@Column(name = "update_time")
	private Date updateTime; 
	
	

}
