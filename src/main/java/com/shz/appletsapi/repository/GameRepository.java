package com.shz.appletsapi.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.Game;


public interface GameRepository extends CrudRepository<Game, Integer> {
	
	Game findByTitle(String title);
	
	List<Game> findByIdent(Integer ident);

}
