package com.shz.appletsapi.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.News;

public interface NewsRepository extends CrudRepository<News,Integer> {
	
    Optional<News> findBytitle(String title);

}
