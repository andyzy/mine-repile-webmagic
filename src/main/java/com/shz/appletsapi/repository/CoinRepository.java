package com.shz.appletsapi.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.Coin;

public interface CoinRepository extends CrudRepository<Coin,Integer> {
	
    Optional<Coin> findByCoinCode(String coinCode);

}
