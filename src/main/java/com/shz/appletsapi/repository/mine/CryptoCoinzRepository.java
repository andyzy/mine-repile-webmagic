package com.shz.appletsapi.repository.mine;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.mine.CryptoCoinz;

public interface CryptoCoinzRepository  extends CrudRepository<CryptoCoinz, Integer> {
	
	CryptoCoinz findByName(String name);
	
	List<CryptoCoinz> findByNameLike(String search);
	
	List<CryptoCoinz> findAll();

}
