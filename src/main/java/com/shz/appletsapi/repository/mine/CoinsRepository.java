package com.shz.appletsapi.repository.mine;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.mine.Coins;

public interface CoinsRepository extends CrudRepository<Coins, Integer> {
	
	 Optional<Coins> findByCoinName(String name);

}
