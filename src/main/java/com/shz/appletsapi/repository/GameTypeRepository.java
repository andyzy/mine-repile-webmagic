package com.shz.appletsapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.GameType;

public interface GameTypeRepository extends CrudRepository<GameType, Integer> {

}
