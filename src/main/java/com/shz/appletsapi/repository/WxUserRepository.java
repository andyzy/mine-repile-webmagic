package com.shz.appletsapi.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.WxUser;

public interface WxUserRepository extends CrudRepository<WxUser, Integer> {
	
	Optional<WxUser>  findByUserCode(String userCode);

}
