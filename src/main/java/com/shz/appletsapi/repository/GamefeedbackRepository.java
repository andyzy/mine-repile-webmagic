package com.shz.appletsapi.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.GameFeedback;

public interface GamefeedbackRepository extends CrudRepository<GameFeedback,Integer> {

	
	List<GameFeedback> findByUserCode(String userCode);
}
