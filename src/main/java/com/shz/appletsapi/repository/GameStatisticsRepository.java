package com.shz.appletsapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.GameStatistics;

public interface GameStatisticsRepository extends CrudRepository<GameStatistics,Integer> {

	

}
