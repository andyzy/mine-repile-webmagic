package com.shz.appletsapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.GameSearch;

public interface GameSearchRepository extends CrudRepository<GameSearch, Integer> {

      GameSearch	 findBySearch(String search);
      
     

}
