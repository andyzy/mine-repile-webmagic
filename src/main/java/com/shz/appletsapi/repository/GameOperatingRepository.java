package com.shz.appletsapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.shz.appletsapi.model.po.GameOperating;

public interface GameOperatingRepository extends CrudRepository<GameOperating, Integer> {
	
	Optional<List<GameOperating>> findByGameIdAndType(Integer gameId,Integer type);
	
	Optional<GameOperating> findByGameIdAndUserCodeAndType(Integer gameId,String userCode,Integer type);
	

}
