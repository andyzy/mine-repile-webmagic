package com.shz.appletsapi.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerExceptionHandler {
	
	
	@ExceptionHandler(ShzException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public Map<String,Object> handlerShzException (ShzException ex){
		 Map<String,Object> result =new HashMap<String,Object>();
		 result.put("status","0");
		 result.put("message", ex.getMessage());
		 return result;
		
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public Map<String,Object> handlerException(Exception ex){
		 Map<String,Object> result =new HashMap<String,Object>();
		 result.put("status","0");
		 result.put("message", ex.getMessage());
		 return result;
		
	}
	

}
