package com.shz.appletsapi.exception;

import com.shz.appletsapi.prop.ShzWebMsg;

public class ShzException  extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7394897210603353854L;
	
	private String message=ShzWebMsg.MSG_FAILURE;
	
	
	
	public ShzException() {
	}

	public ShzException(String message){
		super("shz exception");
		this.message=message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
    
	
	
}
