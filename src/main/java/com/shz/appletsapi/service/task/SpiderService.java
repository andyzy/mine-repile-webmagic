package com.shz.appletsapi.service.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.shz.appletsapi.prop.ShzProperties;
import com.shz.appletsapi.service.webmagic.cryptoCoinz.OnCCPageProcessor;
import com.shz.appletsapi.service.webmagic.mine.OnFxhPNPageProcessor;
import com.shz.appletsapi.service.webmagic.mine.OnFxhPNPipeline;
import com.shz.appletsapi.service.webmagic.mine.OnFxhPricePipeline;
import com.shz.appletsapi.service.webmagic.mine.OnFxhPriceProcessor;
import com.shz.appletsapi.service.webmagic.mine.OnfeixiaohaoPageProcessor;
import com.shz.appletsapi.service.webmagic.mine.OnfeixiaohaoPipeline;
import com.shz.appletsapi.service.webmagic.mine.OnfxhHtmlPageProcessor;
import com.shz.appletsapi.service.webmagic.mine.OnfxhHtmlPipeline;

import lombok.extern.slf4j.Slf4j;
import us.codecraft.webmagic.Spider;

@Component
@Profile(value = "task")
@Slf4j
public class SpiderService {

	@Autowired
	private OnfeixiaohaoPipeline onfeixiaohaoPipeline;

	@Autowired
	private OnFxhPNPipeline onFxhPNPipeline;
	
	@Autowired
	private OnfxhHtmlPipeline onfxhHtmlPipeline;
	
	@Autowired
	private OnFxhPricePipeline onFxhPricePipeline;
	
	@Autowired
	private ShzProperties shzProperties;
	
	@Autowired
	private OnCCPageProcessor onCCPageProcessor;

	// @Scheduled(initialDelay=10000, fixedRate=60*1000*5)
	public void Sendfeixiaohao() {
		Spider.create(new OnfeixiaohaoPageProcessor()).addUrl("https://www.feixiaohao.com/")
				.addPipeline(onfeixiaohaoPipeline).thread(1).run();
	}

	// @Scheduled(initialDelay=10000, fixedRate=60*1000*5)
	public void SendfeixiaohaoNews() {
		Spider.create(new OnFxhPNPageProcessor()).addUrl("https://www.feixiaohao.com/").addPipeline(onFxhPNPipeline)
				.thread(1).run();
	}
	
	//爬非小号页面
	@Scheduled(initialDelay=1000, fixedRate=60*1000*4)
	public void SendfeixiaohaoPage() {
		log.info("-------------SendfeixiaohaoPage  in   starting");
		Spider.create(new OnfxhHtmlPageProcessor())
		.addUrl("https://www.feixiaohao.com/")
	    .addPipeline(onfxhHtmlPipeline).thread(1).run();
		log.info("-------------SendfeixiaohaoPage  in   stop");
	}
	
	//爬非小号币价
	@Scheduled(initialDelay=1000, fixedRate=60*1000*5)
	public void SendonFxhPrice(){
		log.info("-------------SendonFxhPrice  in   starting");
		shzProperties.getReptile().getCoinNames().stream().forEach(s ->{
			Spider.create(new OnFxhPriceProcessor())
			.addUrl("https://www.feixiaohao.com/search?word="+s)
		    .addPipeline(onFxhPricePipeline)
		    .thread(1)
		    .run();
		});
		log.info("-------------SendonFxhPrice  in   stop");
	}
	@Scheduled(initialDelay=1000, fixedRate=60*1000*5)
	public void OnCCPageProcessor() {
		onCCPageProcessor.process();
	}
}