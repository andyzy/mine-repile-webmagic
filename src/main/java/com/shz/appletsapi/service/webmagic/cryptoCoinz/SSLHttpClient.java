package com.shz.appletsapi.service.webmagic.cryptoCoinz;

import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

public class SSLHttpClient {
	public static String getHtml(String url) {
        String html = null;
        for (int i = 1; i <= 3; i++) {
            CloseableHttpClient httpclient = HttpClients.createDefault();// 创建httpClient对象
            HttpGet httpget;
            CloseableHttpResponse response = null;
            httpget = new HttpGet(url);// 以get方式请求该URL
            httpget.addHeader(HttpHeaders.USER_AGENT,
                    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
            RequestConfig requestConfig = RequestConfig.custom()
                    .setSocketTimeout(10000).setConnectTimeout(10000).build();// 设置请求和传输超时时间
            httpget.setConfig(requestConfig);
            try {
                response = httpclient.execute(httpget);// 得到response对象
                int resStatu = response.getStatusLine().getStatusCode();// 返回码
                System.out.println("状态码" + resStatu);
                if (resStatu == HttpStatus.SC_OK) {// 200正常 
                    // 获得相应实体
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        html = EntityUtils.toString(entity, "UTF-8");
                        html = html.replace("&nbsp;", " ");
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                httpclient.getConnectionManager().shutdown();
            }
        }
        return html;
    }
	
	/*public static void main(String[] args) {
		 String html = SSLHttpClient.getHtml("https://www.crypto-coinz.net/master-node-calculator/");
		 Html h =new Html(html);
		 List<Selectable> list= h.xpath("//*[@id=\"tblmd\"]/tbody/tr").nodes();
		 list.remove(0);
		 list.stream().forEach( x -> {
			 System.out.println(x);
			 System.out.println(x.xpath("/tr/td[1]/img//@src").toString());//logo地址
		     System.out.println(x.xpath("/tr/td[2]/font/a/text()").toString());//节点名称	
			 System.out.println(x.xpath("/tr/td[2]/b/text()").toString());//算法
			 System.out.println(x.xpath("/tr/td[3]/font/text()").toString());//节点数
			 
			 System.out.println(x.xpath("/tr/td[4]/span/text()").toString());//价格
			 System.out.println(x.xpath("/tr/td[5]/span[1]/span/text()").toString());//对应比特币数
			 System.out.println(x.xpath("/tr/td[7]/text()").toString());//ROI Days
			 System.out.println(x.xpath("/tr/td[8]/text()").toString());//ROI Years
			 System.out.println(x.xpath("/tr/td[9]/text()").toString());//Coins for Node
			 
			 
			 System.out.println(x.xpath("/tr/td[10]/text()").toString()); //Node Worth
			 System.out.println(x.xpath("/tr/td[11]/text()").toString());   //Coins Daily
			 
			 System.out.println(x.xpath("/tr/td[12]/font[1]/text()").toString());   //Daily 
			 System.out.println(x.xpath("/tr/td[12]/font[2]/text()").toString());   //Weekly
			 
			 System.out.println(x.xpath("/tr/td[13]/font[1]/text()").toString());   //Monthly 
			 System.out.println(x.xpath("/tr/td[13]/font[2]/text()").toString());   //Yearly
			 
			////*[@id="row"]/td[13]/font[1]
			//*[@id="row"]/td[12]/font[2]
		 });
	}  */
}
