package com.shz.appletsapi.service.webmagic.mine;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shz.appletsapi.model.po.Coin;
import com.shz.appletsapi.repository.CoinRepository;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;


@Component
public class OnfeixiaohaoPipeline implements Pipeline {
	
	@Autowired
	private CoinRepository coinRepository;

	@Override
	public void process(ResultItems resultItems, Task task) {
		
		 for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
			if(entry.getKey().equals("coin")) {
				System.out.println(entry.getKey() + ":\t" + entry.getValue());
				Coin coin=(Coin) entry.getValue();
				Coin c=   coinRepository.findByCoinCode(coin.getCoinCode()).orElse(null);
				if(c == null) {
					coin.setCreateTime(new Date());
				}else {
					coin.setId(c.getId());
					coin.setUpdateTime(new Date());
				}
				coinRepository.save(coin);
				
			}
		 }

	}

}
