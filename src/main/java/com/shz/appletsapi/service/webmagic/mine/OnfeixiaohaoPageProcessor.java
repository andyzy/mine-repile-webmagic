package com.shz.appletsapi.service.webmagic.mine;

import java.util.ArrayList;
import java.util.List;

import com.shz.appletsapi.model.po.Coin;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;


public class OnfeixiaohaoPageProcessor implements PageProcessor {

	private Site site = Site
			.me()
			.setRetryTimes(3)
			.setSleepTime(1000)
			.setTimeOut(10*1000);
	
	private static String URL ="^((?!#).)*$";
	private Boolean flag= Boolean.TRUE;

    @Override
    public void process(Page page) {
    	List<String> pages =new ArrayList<>();
    	if(flag) {
    		 pages=page.getHtml().xpath("//tbody//a").links().all();
    		 flag=false;
    	}
    	pages.stream().forEach(System.out::println);
    	System.out.println(pages.stream().count());
    	page.addTargetRequests(pages);
    	if(!page.getUrl().regex(URL).match()) {
    		
    	}else {
    		Coin coin =new Coin();
    		//log
    		coin.setCoinLogo(page.getHtml().xpath("//div[@class='cell maket']/h1/img//@src").toString());
    		//code
    		coin.setCoinCode(page.getHtml().xpath("//div[@class='cell maket']/h1/small/text()").toString());
    		//单价
    		coin.setPrice(page.getHtml().xpath("//div[@class='coinprice']/text()").toString());
    		//24小时最高
    		coin.setPriceDayMax(page.getHtml().xpath("//div[@class='lowHeight']/div[1]/span/text()").toString());
    		//24小时最低
    		coin.setPriceDayMin(page.getHtml().xpath("//div[@class='lowHeight']/div[2]/span/text()").toString());
    		//涨幅
    		coin.setGain(page.getHtml().xpath("//div[@class='coinprice']/span/text()").toString());
    		//简介
    		coin.setDes(page.getHtml().xpath("//div[@class='des']/text()").toString());
    		//流通市值
    		coin.setCoinCmv(page.getHtml().xpath("//div[@class='firstPart']/div[2]/div[2]/text()").toString());
    		//流通市值排名
    		coin.setCoinCmvRank(page.getHtml().xpath("//div[@class='firstPart']/div[2]/div[2]/span/text()").toString());
    		//总流通
    		coin.setCoinTotalC(page.getHtml().xpath("//div[@class='firstPart']/div[3]/div[2]/text()").toString());
    		//总发行量
    		coin.setCoinTotalC2(page.getHtml().xpath("//div[@class='firstPart']/div[3]/div[4]/text()").toString());
    		//24小时成交额
    		coin.setDayTurnover(page.getHtml().xpath("//div[@class='firstPart']/div[4]/div[2]/text()").toString());
    		//24小时成交额排名
    		coin.setDayTurnoverRank(page.getHtml().xpath("//div[@class='firstPart']/div[4]/div[2]/span/text()").toString());
    		//英文名
    		coin.setCoinEn(page.getHtml().xpath("//div[@class='secondPark']/ul/li[1]/span[2]/text()").toString());
    		//中文名
    		coin.setCoinCn(page.getHtml().xpath("//div[@class='secondPark']/ul/li[2]/span[2]/text()").toString());		
    		//上架交易所
    		coin.setExchange(page.getHtml().xpath("//div[@class='secondPark']/ul/li[3]/span[2]/a/text()").toString());
    		//发行时间
    		coin.setPublishTime(page.getHtml().xpath("//div[@class='secondPark']/ul/li[4]/span[2]/text()").toString());
    		//白皮书地址
    		coin.setWhitePaper(page.getHtml().xpath("//div[@class='secondPark']/ul/li[5]/span[2]/a").links().toString());
    		page.putField("coin", coin);
    	  /*  List<Selectable> list=	page.getHtml().xpath("//div[@class='boxContain']/div/ul").nodes();
    	    
    	    list.forEach(s -> {
    	    	System.out.println(s);
    	    	System.out.println(s.xpath("//div[@class='time']/text()").get());
    	    	System.out.println(s.xpath("//div[@class='tit']/span/text()").get());
    	    	System.out.println(s.xpath("//div[@class='tit']/h3/text()").get());
    	    });*/

    		
    		
    	}
    		
    }

    @Override
    public Site getSite() {
        return site;
    }
    
    
    
    
    /*public static void main(String[] args) {

        Spider.create(new OnfeixiaohaoPageProcessor())
                //从"https://github.com/code4craft"开始抓
                .addUrl("https://www.feixiaohao.com/")
                //开启5个线程抓取
                .thread(1)
                //启动爬虫
                .run();
    }*/
}
