package com.shz.appletsapi.service.webmagic;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shz.appletsapi.model.po.Game;
import com.shz.appletsapi.repository.GameRepository;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;


@Component
public class GamePipeline implements Pipeline {
	
	@Autowired
	private GameRepository gameRepository;

	@Override
	public void process(ResultItems resultItems, Task task) {
		
		 for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
			if(entry.getKey().equals("game")) {
				System.out.println(entry.getKey() + ":\t" + entry.getValue());
				Game g=(Game) entry.getValue();
				g.setCreateTime(new Date());
				System.out.println(g.getTitle());	
				if(gameRepository.findByTitle(g.getTitle()) == null) {
					gameRepository.save(g);	
				}
			}
		 }

	}

}
