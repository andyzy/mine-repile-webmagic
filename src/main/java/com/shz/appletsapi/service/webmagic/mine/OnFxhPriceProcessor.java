package com.shz.appletsapi.service.webmagic.mine;

import com.shz.appletsapi.model.po.mine.Coins;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

public class OnFxhPriceProcessor implements PageProcessor {

	private Site site = Site.me().setRetryTimes(3).setSleepTime(1000).setTimeOut(10 * 1000);

	@Override
	public void process(Page page) {

		String price = page.getHtml().xpath("//*[@id=\"table\"]/tbody/tr/td[4]/text()").get();
		page.putField("price", price.trim());
		String url = page.getUrl().get();
		url = url.replace("https://www.feixiaohao.com/search?word=", "");
		Coins c = new Coins();
		c.setPrice(price);
		c.setCoinName(url);
		page.putField("coins", c);

	}

	@Override
	public Site getSite() {
		return site;
	}

}
