package com.shz.appletsapi.service.webmagic.screenshots;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;


public class OnWxappTopProcessor implements PageProcessor {

	private Site site = Site
			.me()
			.setRetryTimes(3)
			.setSleepTime(1000);
	
	private static String URL ="https://wxapp.top/app/[\\s\\S]*";
	
	
    @Override
    public void process(Page page) {
    	
    
    	String infoHtml = page.getHtml().xpath("//div[@class='search_result_cell_name']/a").links().toString();
    	if(StringUtils.isNotBlank(infoHtml)) {
    		page.addTargetRequest(infoHtml);
    	}
    	if(page.getUrl().regex(URL).match()) {
    		List<String> list=page.getHtml().xpath("//div[@class='row app_sc']/div/a").links().all();
    		page.putField("name",page.getHtml().xpath("//div[@class='col-md-12']/div/h2/text()").toString());
    		page.putField("screenshots", StringUtils.join(list,","));
    	}
    	

    }

    @Override
    public Site getSite() {
        return site;
    }
    
    
   /* public static void main(String[] args) {
    	  Spider
	        .create(new OnWxappTopProcessor())
	        .addUrl("https://wxapp.top/?c=search&sc=糖果之森")
	        .run();
	}*/
    
    
}
