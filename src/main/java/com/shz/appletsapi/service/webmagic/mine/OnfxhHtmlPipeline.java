package com.shz.appletsapi.service.webmagic.mine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shz.appletsapi.prop.ShzProperties;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;


@Component
public class OnfxhHtmlPipeline implements Pipeline {
	
	@Autowired
	private ShzProperties shzProperties;
	

	@Override
	public void process(ResultItems resultItems, Task task) {
		String h ="";
		String name ="";
		 for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
			if(entry.getKey().equals("html")) {
				 h=(String) entry.getValue();
			}else if(entry.getKey().equals("name")) {
				name=(String) entry.getValue();
			}
		 }
		 if(StringUtils.isNotBlank(h)&&StringUtils.isNotBlank(name)) {
			 String indexUrl =shzProperties.getReptile().getPath()+"/index.html";
			 File file = new File(indexUrl);
			 if(name.equals("rvn")) {
				 StringBuffer s1 = new StringBuffer(getHtml(file));
				 String s2 = h;
				 Pattern p = Pattern.compile("<tr id=\"bitcoin\">"); 		
				 Matcher m = p.matcher(s1.toString());
				 if(m.find()){
					 s1.insert((m.start()), s2);
				 }
				 saveHtml(indexUrl,s1.toString());			 
			 }else if(name.equals("bcx")) {
				 StringBuffer s1 = new StringBuffer(getHtml(file));
				 String s2 = h;
				 Pattern p = Pattern.compile("<tr id=\"bitcoin\">"); 		
				 Matcher m = p.matcher(s1.toString());		
				 if(m.find()){
					 s1.insert((m.start()), s2);
				 }
				 saveHtml(indexUrl,s1.toString());	 
			 }else if(name.equals("index")) {
				 StringBuffer s = new StringBuffer(h);
				 Pattern p = Pattern.compile("<div class=\"new-wrapper\">"); 		
				 Matcher m = p.matcher(s.toString());
				 if(m.find()){
					 s.insert((m.start()),"<link href=\"index.css\" rel=\"stylesheet\">");
					 s.insert((m.start()),"<script src=\"index.js\"></script>");
					 s.insert((m.start())," <img class='logding' src='http://hq.sosmart.site/img/load.gif'/>");
				 }
		
				 Pattern p2 = Pattern.compile("<table class=\"new-table new-coin-list\" id=\"table\">"); 		
				 Matcher m2 = p2.matcher(s.toString());
				 if(m2.find()){
					 s.insert((m2.start()),"<div class=\"group_box\">\r\n" + 
					 		"          	<div class=\"input-group\">\r\n" + 
					 		"	               <input type=\"text\" id=\"name\" onkeyup=\"search123()\" placeholder=\"请输入搜索名字\" >\r\n" + 
					 		"	          </div>\r\n" + 
					 		"       <input class=\"reload\" type=button value=\"\" onclick=\"location.reload()\">     \r\n" + 
					 		"    </div> ");
				 }
				 
				
						 
				 saveHtml(shzProperties.getReptile().getPath()+"/"+name+".html",s.toString());
			 }
			 else {
				 StringBuffer s = new StringBuffer(h);
				 Pattern p = Pattern.compile("<div class=\"m15\">"); 		
				 Matcher m = p.matcher(s.toString());
				 if(m.find()){
					 s.insert((m.start()),"<meta name=\"viewport\" content=\"width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no\" />"); 
					 s.insert((m.start()),"<script src=\"detail.js\"></script>");
					 s.insert((m.start()),"<script src=\"//static.feixiaohao.com/Scripts/layer/layer.js\"></script>");
					 s.insert((m.start()),"<link href=\"detail.css\" rel=\"stylesheet\">");
					 s.insert((m.start()),"<div  class=\"back\"><a  onclick=\"window.history.go(-1);\"><img src=\"../img/goback.png\"></a></div>"); 
					
				 }
				 saveHtml(shzProperties.getReptile().getPath()+"/"+name+".html",s.toString());
			 }
		 }
	}
	
	 public static void saveHtml(String filepath, String str){
	        try {
	            OutputStreamWriter outs = new OutputStreamWriter(new FileOutputStream(filepath, false), "utf-8");
	            outs.write(str);
	            outs.close();
	        } catch (IOException e) {
	            System.out.println("Error at save html...");
	            e.printStackTrace();
	        }
	    }
	 
	 public static String getHtml(File file){
	        StringBuilder result = new StringBuilder();
	        try{
	            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
	            String s = null;
	            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
	                result.append(System.lineSeparator()+s);
	            }
	            br.close();    
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return result.toString();
	    }
	 
	/* public static void main(String[] args){
	        File file = new File("./static/index2.html");
	        System.out.println(getHtml(file));
	    }*/

}
