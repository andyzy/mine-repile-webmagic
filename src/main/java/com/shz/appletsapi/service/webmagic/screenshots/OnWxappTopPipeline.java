package com.shz.appletsapi.service.webmagic.screenshots;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shz.appletsapi.model.po.Game;
import com.shz.appletsapi.repository.GameRepository;

import lombok.extern.slf4j.Slf4j;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

@Component
@Slf4j
public class OnWxappTopPipeline implements Pipeline {

	@Autowired
	private GameRepository gameRepository;

	@Override
	public void process(ResultItems resultItems, Task task) {
		String name ="";
		String screenshots="";
		
		 for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
			 if(entry.getKey().equals("name")) {
				name = entry.getValue().toString(); 
			 }else if(entry.getKey().equals("screenshots")) {
				screenshots= entry.getValue().toString(); 
			 }
		 }
		 
		 if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(screenshots)) {
			 log.info("name:"+name.trim());
			 Game game =gameRepository.findByTitle(name.trim());
			 game.setIdent(1);
			 game.setScreenshots(screenshots);
			 gameRepository.save(game);
		 }

	}
	
    
    
}
