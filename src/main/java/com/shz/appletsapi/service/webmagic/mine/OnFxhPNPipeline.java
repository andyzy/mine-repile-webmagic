package com.shz.appletsapi.service.webmagic.mine;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shz.appletsapi.model.po.Coin;
import com.shz.appletsapi.model.po.News;
import com.shz.appletsapi.repository.CoinRepository;
import com.shz.appletsapi.repository.NewsRepository;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;


@Component
public class OnFxhPNPipeline implements Pipeline {
	
	@Autowired
	private NewsRepository newsRepository;

	@Override
	public void process(ResultItems resultItems, Task task) {
		
		 for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
			if(entry.getKey().equals("news")) {
				System.out.println(entry.getKey() + ":\t" + entry.getValue());
				News news=(News) entry.getValue();
				if(newsRepository.findBytitle(news.getTitle().trim()).orElse(null) == null) {
					news.setCreateTime(new Date());
					news.setCreateDate(new Date());
					newsRepository.save(news);
				}
				
				
				
			}
		 }

	}

}
