package com.shz.appletsapi.service.webmagic.cryptoCoinz;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shz.appletsapi.model.po.mine.CryptoCoinz;
import com.shz.appletsapi.repository.mine.CryptoCoinzRepository;

import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

@Service
public class OnCCPageProcessor  {
	
	
	private final CryptoCoinzRepository cryptoCoinzRepository;
	
	public OnCCPageProcessor(CryptoCoinzRepository cryptoCoinzRepository) {
		this.cryptoCoinzRepository = cryptoCoinzRepository;
	}
	
	
	public void process() {
		 String html = SSLHttpClient.getHtml("https://www.crypto-coinz.net/master-node-calculator/");
		 Html h =new Html(html);
		 List<Selectable> list= h.xpath("//*[@id=\"tblmd\"]/tbody/tr").nodes();
		 list.remove(0);
		 list.stream().forEach( x -> {
			 
			 CryptoCoinz cryptoCoinz = new CryptoCoinz();
			 String logoSrc = x.xpath("/tr/td[1]/img//@src").toString();
			 logoSrc=logoSrc.substring(2,logoSrc.length());
			 cryptoCoinz.setLogoSrc("https://www.crypto-coinz.net"+logoSrc);//logo地址
			 cryptoCoinz.setName(x.xpath("/tr/td[2]/font/a/text()").toString());//节点名称	
			 cryptoCoinz.setDiff(x.xpath("/tr/td[2]/b/text()").toString());//算法
			 cryptoCoinz.setNodes(x.xpath("/tr/td[3]/font/text()").toString());//节点数
			 
			 cryptoCoinz.setPrice(x.xpath("/tr/td[4]/span/text()").toString());//价格
			 cryptoCoinz.setPriceBtc(x.xpath("/tr/td[5]/span[1]/span/text()").toString());//对应比特币数
			 cryptoCoinz.setRoiDays(x.xpath("/tr/td[7]/text()").toString());//ROI Days
			 cryptoCoinz.setRoiYearly(x.xpath("/tr/td[8]/text()").toString());//ROI Years
			 cryptoCoinz.setCoinsForNode(x.xpath("/tr/td[9]/text()").toString());//Coins for Node
			 
			 
			 cryptoCoinz.setNodeWorth(x.xpath("/tr/td[10]/text()").toString()); //Node Worth
			 cryptoCoinz.setCoinsDaily(x.xpath("/tr/td[11]/text()").toString());   //Coins Daily
			 
			 cryptoCoinz.setDaily(x.xpath("/tr/td[12]/font[1]/text()").toString());   //Daily 
			 cryptoCoinz.setWeekly(x.xpath("/tr/td[12]/font[2]/text()").toString());   //Weekly
			 
			 cryptoCoinz.setMonthly(x.xpath("/tr/td[13]/font[1]/text()").toString());   //Monthly 
			 cryptoCoinz.setYearly(x.xpath("/tr/td[13]/font[2]/text()").toString());   //Yearly
			
			 CryptoCoinz cc=cryptoCoinzRepository.findByName( cryptoCoinz.getName());
			 if( cc == null) {
				 cryptoCoinz.setCreateDate(new Date());
			 }else {
				 cryptoCoinz.setUpdateDate(new Date());
				 cryptoCoinz.setCreateDate(cc.getCreateDate());
				 cryptoCoinz.setId(cc.getId());
			 }
			 cryptoCoinzRepository.save(cryptoCoinz);
		 });
		
	}

}
