package com.shz.appletsapi.service.webmagic.screenshots;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;


public class OnDamengxiangProcessor implements PageProcessor {

	private Site site = Site
			.me()
			.setRetryTimes(3)
			.setSleepTime(1000);
	
	private static String URL ="https://www.damengxiang.me/app/[\\s\\S]*.html";
	
	private ObjectMapper mapper =new ObjectMapper();
	
    @Override
    public void process(Page page) {
    	
    
    	String infoHtml = page.getHtml().xpath("//div[@class='minapps']/ul/li/a").links().toString();
    	
    	
    	if(StringUtils.isNotBlank(infoHtml)) {
    		page.addTargetRequest(infoHtml);
    	}
    	if(page.getUrl().regex(URL).match()) {
    		
    		Pattern r = Pattern.compile(".*xcx.*");
    	      // 现在创建 matcher 对象
    	      Matcher m = r.matcher(page.getHtml().toString());
    		  if (m.find()) {
				String s =m.group().replace("xcx:","");
			    try {
			    	LinkedHashMap o=(LinkedHashMap) mapper.readValue(s.substring(0,s.length() - 1),Object.class);
					if(o.get("screenshot") != null) {
						List<String> jtList=(List<String>) o.get("screenshot");
						String name =page.getHtml().xpath("//title/text()").toString();
						name =name.substring(0, name.indexOf("，"));
						page.putField("name",name);
						page.putField("screenshots", StringUtils.join(jtList,","));
					}		
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   
			}	
    	}		
    }

    @Override
    public Site getSite() {
        return site;
    }
    
    
  /*  public static void main(String[] args) {
    	  Spider
	        .create(new OnDamengxiangProcessor())
	        .addUrl("https://www.damengxiang.me/search/跳一跳.html")
	        .run();
	}*/
    
    
}
