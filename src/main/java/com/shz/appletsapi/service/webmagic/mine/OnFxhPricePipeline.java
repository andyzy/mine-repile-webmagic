package com.shz.appletsapi.service.webmagic.mine;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shz.appletsapi.model.po.mine.Coins;
import com.shz.appletsapi.repository.mine.CoinsRepository;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;


@Component
public class OnFxhPricePipeline implements Pipeline {
	
	@Autowired
	private CoinsRepository coinsRepository;

	@Override
	public void process(ResultItems resultItems, Task task) {
		
		 for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
			if(entry.getKey().equals("coins")) {
				System.out.println(entry.getKey() + ":\t" + entry.getValue());
				Coins coins=(Coins) entry.getValue();
				Coins coins2 =coinsRepository.findByCoinName(coins.getCoinName()).orElse(null);
				if(coins2==null) {
					coins.setCreateTime(new Date());
				}else {
					coins.setCreateTime(coins2.getCreateTime());
					coins.setUpdateTime(new Date());
					coins.setId(coins2.getId());
				}
				coinsRepository.save(coins);
				
			}
		 }

	}

}
